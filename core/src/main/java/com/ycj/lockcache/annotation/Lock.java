/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ycj.lockcache.annotation;


import com.ycj.lockcache.enums.LockType;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 分布式锁注解
 *
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Lock {


	/**
	 * 锁的前置KEY,结合spring cache注解中的key作为最终的key
	 * 比如：preKey为redisson:lock:,@Cacheable(key="123456")
	 * 最终的锁对应的key为redisson:lock:123456
	 * @return
	 */
	String preKey() default "redisson-lock-";

	/**
	 * 锁类型
	 * @return LockType
	 */
	LockType lockType() default LockType.REENTRANT_LOCK;

	/**
	 * 锁超时时间/单位秒，默认3S
	 * @return
	 */
	long timeOut() default 3L;

}
