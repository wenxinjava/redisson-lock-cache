package com.ycj.lockcache.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author changjin.yuan
 * @Date 2019/8/30 19:04
 */


@Configuration
public class RedissonConfig {

    @Value("${spring.redis.address}")
    private  String address;

    @Value("${spring.redis.password}")
    private String password;

    @Bean
    public RedissonClient redissonClient() {
        RedissonClient redisson = null;
        Config config = new Config();

        //config.useSentinelServers().addSentinelAddress("redis://10.6.1.23:6475", "redis://10.6.1.23:6476", "redis://10.6.1.52:6475").setPassword("4boKTwmsOcc6UHQk").setMasterName("sentinel-10.6.1.23-6474");

        config.useSingleServer()
                .setAddress(address)
                .setPassword(password);
        redisson = Redisson.create(config);

        //可通过打印redisson.getConfig().toJSON().toString()来检测是否配置成功
        return redisson;

    }

}
