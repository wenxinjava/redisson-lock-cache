package com.ycj.lockcache.lock;

import org.redisson.api.RLock;

/**
 *
 * @author changjin.yuan
 * @Date 2019/9/2 14:40
 */
public interface DistributedLock {

    /**
     *
     * @param lockKey
     * @param timeout
     * @return
     */
    RLock lock(String lockKey, long timeout);

    /**
     * unlock
     * @param lockKey
     */
    void unlock(String lockKey);

    /**
     * unlock
     * @param lock
     */
    void unlock(RLock lock);

}
